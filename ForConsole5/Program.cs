﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole5
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, tmp_ch, res=0;
            string tmp;
            Console.WriteLine("Введiть N");
            tmp = Console.ReadLine();
            a = double.Parse(tmp);

            for(int i=0;i<a;i++)
            {
                tmp_ch = 1;
                for(int o=1;o<=1+i;o++)
                {
                    tmp_ch *= o;
                }
                res += tmp_ch;
            }

            Console.WriteLine("Вiдповiдь =  {0}", res);
        }
    }
}
