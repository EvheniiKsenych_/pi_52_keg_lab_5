﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            double result = 0;
            string tmp;
            Console.WriteLine("Введiть N");
            tmp = Console.ReadLine();
            N = int.Parse(tmp);
            for (int i = 1; i <= N; i++)
            {
                result += (1.0 / i);
            }
            Console.WriteLine("Вiдповiдь =  {0}", result);
        }
    }
}
